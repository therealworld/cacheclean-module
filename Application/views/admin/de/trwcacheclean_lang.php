<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

include_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR .
    'de' .
    DIRECTORY_SEPARATOR . basename(__FILE__);

$aLang = array_merge(
    $aLang,
    [
        'mxtrwcacheclean' => 'Cache leeren',

        'TRWCACHECLEAN'           => 'Cache leeren',
        'TRWCACHECLEAN_AUTOMATIC' => 'Automatisch starten',
        'TRWCACHECLEAN_MANUELL'   => 'Manuell starten',
        'TRWCACHECLEAN_CACHE'     => 'Cache leeren?',
        'TRWCACHECLEAN_VIEWS'     => 'Views updaten?',
        'TRWCACHECLEAN_IMAGES'    => 'Bilder zurücksetzen?',
        'TRWCACHECLEAN_SEOURLS'   => 'Dynamisch erzeugte Seo-Urls zurücksetzen?',
        'TRWCACHECLEAN_TPLBLOCKS' => 'Tpl Blocks aufräumen?',
        'TRWCACHECLEAN_MODULES'   => 'Module-Cache leeren?',
        'TRWCACHECLEAN_ACTIVE'    => 'Cache leeren aktiv? <b>%s</b>, Views updaten aktiv? <b>%s</b>, Bilder zurücksetzen aktiv? <b>%s</b>, dyn. Seo-Urls zurücksetzen aktiv? <b>%s</b>, TplBlocks aufräumen aktiv? <b>%s</b>',
    ]
);
