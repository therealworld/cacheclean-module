<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwcacheclean' => 'Automatisch starten',

    'SHOP_MODULE_bTRWCacheCleanCache'     => 'Cache leeren?',
    'SHOP_MODULE_bTRWCacheCleanViews'     => 'Views updaten?',
    'SHOP_MODULE_bTRWCacheCleanImages'    => 'Bilder zurücksetzen?',
    'SHOP_MODULE_bTRWCacheCleanSeoUrls'   => 'Dynamisch erzeugte Seo-Urls zurücksetzen?',
    'SHOP_MODULE_bTRWCacheCleanTplBlocks' => 'Tpl Blocks aufräumen?',
];
