<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwcacheclean' => 'Start automatic',

    'SHOP_MODULE_bTRWCacheCleanCache'     => 'Cache clean?',
    'SHOP_MODULE_bTRWCacheCleanViews'     => 'Update Views?',
    'SHOP_MODULE_bTRWCacheCleanImages'    => 'Reset Images?',
    'SHOP_MODULE_bTRWCacheCleanSeoUrls'   => 'Reset dynamically generated Seo Urls?',
    'SHOP_MODULE_bTRWCacheCleanTplBlocks' => 'clean Tpl Blocks',
];
