<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

include_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR .
    'en' .
    DIRECTORY_SEPARATOR . basename(__FILE__);

$aLang = array_merge(
    $aLang,
    [
        'mxtrwcacheclean' => 'Cache Clean',

        'TRWCACHECLEAN'           => 'Cache Clean',
        'TRWCACHECLEAN_AUTOMATIC' => 'Start automatic',
        'TRWCACHECLEAN_MANUELL'   => 'Start manuell',
        'TRWCACHECLEAN_CACHE'     => 'Cache clean?',
        'TRWCACHECLEAN_VIEWS'     => 'Update Views?',
        'TRWCACHECLEAN_IMAGES'    => 'Reset Images?',
        'TRWCACHECLEAN_SEOURLS'   => 'Reset dynamically generated Seo Urls?',
        'TRWCACHECLEAN_TPLBLOCKS' => 'Reset TplBlocks?',
        'TRWCACHECLEAN_MODULES'   => 'Clear Module-Caches?',
        'TRWCACHECLEAN_ACTIVE'    => 'Cache active ? <b>%s</b>, views update active ? <b>%s</b>, reset pictures active ? <b>%s</b>, reset dyn. Seo-Urls active? <b>%s</b>, reset TplBlocks active? <b>%s</b>',
    ]
);
