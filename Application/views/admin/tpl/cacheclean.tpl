[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]
<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="" />
    <input type="hidden" name="cl" value="" />
</form>
<h1>[{oxmultilang ident="TRWCACHECLEAN"}]</h1>
[{foreach from=$aResults key=sType item=bResult}]
    [{assign var=sMessageIdent value="TRWCACHECLEAN_"|cat:$sType|cat:"_SUCCESS"}]
    <p class="[{if $bResult}]message[{else}]error[{/if}]box">
        [{oxmultilang ident=$sMessageIdent suffix=":"}]
        [{if $bResult}][{oxmultilang ident="GENERAL_YES"}][{else}][{oxmultilang ident="GENERAL_NO"}][{/if}]
    </p>
[{/foreach}]

<h2>[{oxmultilang ident="TRWCACHECLEAN_AUTOMATIC"}]</h2>
<p>[{$sActive}]</p>

[{if $aManuellUrls|@count}]
    <h2>[{oxmultilang ident="TRWCACHECLEAN_MANUELL"}]</h2>
    [{foreach from=$aManuellUrls key=sType item=sUrl}]
        [{assign var=sMessageIdent value="TRWCACHECLEAN_"|cat:$sType}]
        [{assign var=sHelpIdent value="HELP_"|cat:$sMessageIdent}]
        <p>
            <a href="[{$sUrl}]">[{oxmultilang ident=$sMessageIdent}]</a> [{oxinputhelp ident=$sHelpIdent}]
        </p>
    [{/foreach}]
[{/if}]
[{include file="bottomitem.tpl"}]