<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CacheCleanModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsCache;

/**
 * provides a Cache Cleaner
 * Admin Menu: Service -> Cache Cleaner.
 */
class CacheCleanController extends AdminDetailsController
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'cacheclean.tpl';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render()
    {
        $oConfig = Registry::getConfig();
        $oLang = Registry::getLang();
        $bCache = $oConfig->getConfigParam('bTRWCacheCleanCache');
        $bViews = $oConfig->getConfigParam('bTRWCacheCleanViews');
        $bImages = $oConfig->getConfigParam('bTRWCacheCleanImages');
        $bSeoUrls = $oConfig->getConfigParam('bTRWCacheCleanSeoUrls');
        $bTplBlocks = $oConfig->getConfigParam('bTRWCacheCleanTplBlocks');
        $bModules = $oConfig->getConfigParam('bTRWCacheCleanModules');

        $this->_aViewData['sActive'] = sprintf(
            $oLang->translateString('TRWCACHECLEAN_ACTIVE'),
            $oLang->translateString('GENERAL_' . ($bCache ? 'YES' : 'NO')),
            $oLang->translateString('GENERAL_' . ($bViews ? 'YES' : 'NO')),
            $oLang->translateString('GENERAL_' . ($bImages ? 'YES' : 'NO')),
            $oLang->translateString('GENERAL_' . ($bSeoUrls ? 'YES' : 'NO')),
            $oLang->translateString('GENERAL_' . ($bTplBlocks ? 'YES' : 'NO')),
            $oLang->translateString('GENERAL_' . ($bModules ? 'YES' : 'NO'))
        );

        $sBaseLink = $this->getViewConfig()->getSelfLink() . 'cl=CacheCleanController&fnc=';

        $aManuellUrls = [];

        // clean cache
        if ($bCache) {
            $this->runCleanCache();
        } else {
            $aManuellUrls['CACHE'] = $sBaseLink . 'runCleanCache';
        }

        // update views
        if ($bViews) {
            $this->runUpdateViews();
        } else {
            $aManuellUrls['VIEWS'] = $sBaseLink . 'runUpdateViews';
        }

        // reset images
        if ($bImages) {
            $this->runResetImages();
        } else {
            $aManuellUrls['IMAGES'] = $sBaseLink . 'runResetImages';
        }

        // clean dynamic Seo Urls
        if ($bSeoUrls) {
            $this->runCleanDynamicSeoUrls();
        } else {
            $aManuellUrls['SEOURLS'] = $sBaseLink . 'runCleanDynamicSeoUrls';
        }

        // clean dynamic Seo Urls
        if ($bTplBlocks) {
            $this->runResetTplBlocks();
        } else {
            $aManuellUrls['TPLBLOCKS'] = $sBaseLink . 'runResetTplBlocks';
        }

        // cleanup the Modules
        if ($bModules) {
            $this->runCleanUpModules();
        } else {
            $aManuellUrls['MODULES'] = $sBaseLink . 'runCleanUpModules';
        }

        $this->_aViewData['aManuellUrls'] = $aManuellUrls;

        return $this->_sThisTemplate;
    }

    /** run Cache Clean */
    public function runCleanCache(): void
    {
        ToolsCache::cleanCache();
        $this->_aViewData['aResults']['CACHE'] = true;
    }

    /** run Update Views */
    public function runUpdateViews(): void
    {
        ToolsCache::updateViews();
        $this->_aViewData['aResults']['VIEWS'] = true;
    }

    /** run Reset Images */
    public function runResetImages(): void
    {
        ToolsCache::resetImages();
        $this->_aViewData['aResults']['IMAGES'] = true;
    }

    /** run clean dynamic Seo Urls */
    public function runCleanDynamicSeoUrls(): void
    {
        ToolsCache::cleanDynamicSeoUrls();
        $this->_aViewData['aResults']['SEOURLS'] = true;
    }

    /** run reset Tpl Blocks */
    public function runResetTplBlocks(): void
    {
        ToolsCache::resetTplBlocks();
        $this->_aViewData['aResults']['TPLBLOCKS'] = true;
    }

    /** run cleanup the Modules */
    public function runCleanUpModules(): void
    {
        ToolsCache::cleanUpModules();
        $this->_aViewData['aResults']['MODULES'] = true;
    }
}
