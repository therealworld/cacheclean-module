<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'TASKCACHECLEAN' => 'Cache clean',

    'TRWCACHECLEAN_CACHE_SUCCESS'     => 'Cleared cache successfully',
    'TRWCACHECLEAN_VIEWS_SUCCESS'     => 'Views Update successful',
    'TRWCACHECLEAN_IMAGES_SUCCESS'    => 'Reset images successfully',
    'TRWCACHECLEAN_SEOURLS_SUCCESS'   => 'Reset SEO-URLs successfully',
    'TRWCACHECLEAN_TPLBLOCKS_SUCCESS' => 'Reset TplBlocks successfully',
    'TRWCACHECLEAN_MODULES_SUCCESS'   => 'Cleared Module-Caches successfully',
];
