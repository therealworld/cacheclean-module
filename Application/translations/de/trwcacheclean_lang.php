<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'TASKCACHECLEAN' => 'Cache leeren',

    'TRWCACHECLEAN_CACHE_SUCCESS'     => 'Cache erfolgreich geleert',
    'TRWCACHECLEAN_VIEWS_SUCCESS'     => 'Views Update erfolgreich',
    'TRWCACHECLEAN_IMAGES_SUCCESS'    => 'Bilder erfolgreich zurückgesetzt',
    'TRWCACHECLEAN_SEOURLS_SUCCESS'   => 'SEO-URLs erfolgreich zurückgesetzt',
    'TRWCACHECLEAN_TPLBLOCKS_SUCCESS' => 'Tpl-Blocks erfolgreich aufgeräumt',
    'TRWCACHECLEAN_MODULES_SUCCESS'   => 'Module-Caches erfolgreich aufgeräumt',
];
