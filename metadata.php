<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use TheRealWorld\CacheCleanModule\Application\Controller\Admin\CacheCleanController;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwcacheclean',
    'title' => [
        'de' => 'the-real-world - Cache leeren',
        'en' => 'the-real-world - Cache Clean',
    ],
    'description' => [
        'de' => 'Leert den Cache.',
        'en' => 'Clean the cache.',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwcacheclean'),
    'author'    => 'Mario Lorenz',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'events'    => [
        'onActivate'   => '\TheRealWorld\CacheCleanModule\Core\CacheCleanEvents::onActivate',
        'onDeactivate' => '\TheRealWorld\CacheCleanModule\Core\CacheCleanEvents::onDeactivate',
    ],
    'controllers' => [
        'CacheCleanController' => CacheCleanController::class,
    ],
    'templates' => [
        'cacheclean.tpl' => 'trw/trwcacheclean/Application/views/admin/tpl/cacheclean.tpl',
    ],
    'settings' => [
        [
            'group' => 'trwcacheclean',
            'name'  => 'bTRWCacheCleanCache',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwcacheclean',
            'name'  => 'bTRWCacheCleanViews',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwcacheclean',
            'name'  => 'bTRWCacheCleanImages',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwcacheclean',
            'name'  => 'bTRWCacheCleanSeoUrls',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwcacheclean',
            'name'  => 'bTRWCacheCleanTplBlocks',
            'type'  => 'bool',
            'value' => false,
        ],
    ],
];
