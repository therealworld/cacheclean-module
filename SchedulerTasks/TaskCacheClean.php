<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CacheCleanModule\SchedulerTasks;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\SchedulerModule\Core\ISchedulerTask;
use TheRealWorld\ToolsPlugin\Core\ToolsCache;
use TheRealWorld\ToolsPlugin\Core\ToolsLog;

/**
 * CacheClean Task Class.
 */
class TaskCacheClean implements ISchedulerTask
{
    /** String of a unix-style crontab */
    protected string $_sDefaultCrontab = '0 0 1 * *'; // monthly, at 12:00AM on the first of every month

    /** Number of the next step */
    protected int $_iNextStep = 0;

    /** Number of the next sub step */
    protected int $_iNextSubStep = 0;

    /** Returns the default crontab of the Task */
    public function getDefaultCrontab(): string
    {
        return $this->_sDefaultCrontab;
    }

    /**
     * get Path for "manual" Files.
     *
     * @param string $sPathKey - a possible key for several paths
     */
    public function getPathForManualFiles(string $sPathKey = ''): string
    {
        return '';
    }

    /**
     * get List of "manual" Files.
     *
     * @param string $sPathKey - a possible key for several paths
     */
    public function getManualFileList(string $sPathKey = ''): array
    {
        return [];
    }

    /** Install the Task */
    public function install(): bool
    {
        return true;
    }

    /**
     * Run the Task.
     *
     * @param int  $iCurrentStep    - The number of the current step
     * @param int  $iCurrentSupStep - The number of the current sub step
     * @param bool $bRunManually    - run the task manually?
     */
    public function run(int $iCurrentStep = 0, int $iCurrentSupStep = 0, bool $bRunManually = false): bool
    {
        $this->_iNextStep = $iCurrentStep;
        $this->_iNextSubStep = $iCurrentSupStep;

        $oConfig = Registry::getConfig();
        $oLang = Registry::getLang();
        $bCache = $oConfig->getConfigParam('bTRWCacheCleanCache');
        $bViews = $oConfig->getConfigParam('bTRWCacheCleanViews');
        $bImages = $oConfig->getConfigParam('bTRWCacheCleanImages');
        $bSeoUrls = $oConfig->getConfigParam('bTRWCacheCleanSeoUrls');
        $bTplBlocks = $oConfig->getConfigParam('bTRWCacheCleanTplBlocks');

        // clean cache
        if ($bCache) {
            ToolsCache::cleanCache();

            ToolsLog::setLogEntry(
                $oLang->translateString('TRWCACHECLEAN_CACHE_SUCCESS') . $oLang->translateString('GENERAL_YES'),
                __CLASS__ . ' - ' . __FUNCTION__,
                'info'
            );
        }

        // update views
        if ($bViews) {
            ToolsCache::updateViews();

            ToolsLog::setLogEntry(
                $oLang->translateString('TRWCACHECLEAN_VIEWS_SUCCESS') . $oLang->translateString('GENERAL_YES'),
                __CLASS__ . ' - ' . __FUNCTION__,
                'info'
            );
        }

        // reset images
        if ($bImages) {
            ToolsCache::resetImages();

            ToolsLog::setLogEntry(
                $oLang->translateString('TRWCACHECLEAN_IMAGES_SUCCESS') . $oLang->translateString('GENERAL_YES'),
                __CLASS__ . ' - ' . __FUNCTION__,
                'info'
            );
        }

        // clean dynamic Seo Urls
        if ($bSeoUrls) {
            ToolsCache::cleanDynamicSeoUrls();

            ToolsLog::setLogEntry(
                $oLang->translateString('TRWCACHECLEAN_SEOURLS_SUCCESS') . $oLang->translateString('GENERAL_YES'),
                __CLASS__ . ' - ' . __FUNCTION__,
                'info'
            );
        }

        // clean dynamic Seo Urls
        if ($bTplBlocks) {
            ToolsCache::resetTplBlocks();

            ToolsLog::setLogEntry(
                $oLang->translateString('TRWCACHECLEAN_TPLBLOCKS_SUCCESS') . $oLang->translateString('GENERAL_YES'),
                __CLASS__ . ' - ' . __FUNCTION__,
                'info'
            );
        }

        return true;
    }

    /**
     * Get the number of the next step.
     *
     * @param bool $bSub - The Sub-Step?
     */
    public function getNextStep(bool $bSub = false): int
    {
        return $bSub ? $this->_iNextSubStep : $this->_iNextStep;
    }
}
