<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

/**
 * @author    Mario Lorenz, www.the-real-world.de
 * @copyright 2020 the-real-world.de
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

namespace TheRealWorld\CacheCleanModule\Core;

class CacheCleanEvents
{
    /**
     * Actions and SQLs during Installation.
     *
     * @return bool
     */
    public static function onActivate()
    {
        return true;
    }

    /**
     * Actions and SQLs during DeInstallation.
     *
     * @return bool
     */
    public static function onDeactivate()
    {
        return true;
    }
}
